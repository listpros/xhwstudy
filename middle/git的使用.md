# git使用

#### git 官网： https://git-scm.com/
#### 官方文档 https://git-scm.com/book/zh/v1/%E8%B5%B7%E6%AD%A5

1. 什么是git、优点 
2. 名词解释
	- 仓库
	- 克隆
	- 分支
	- 暂存
	- 提交
	- 推送
	- 拉取
	- 合并
	- fork

3. 初始化一个本地项目为例
``` 
git init
git status
git add .
git commit -m ""
git show
git log
git log --p
git log --oneline


```

4. 初始化一个远程项目为例

5. 远程仓库与本地仓库建立关系
``` 
git clone [git_url]
git remote add origin [git_url]
git pull origin [branch] //origin为默认名字，可以随意改
git push origin [branch]

```

6.其他
	- issue
	- 合作

