# 写一个jq插件

#### 

1. $.extend:
	- 挂载到全局 ` $.extend({ hello:function(){} }) `
	- 挂载到某个命名空间 ` $.fn.extend({})挂载到实例对象上，既每个不同的$(".cls")对象 `

2. 参考jq extend方法 拓展参数 https://www.cnblogs.com/zikai/p/5074686.html
	``` 
		a. $.extend({
			hello:function(){}
		})//只有一个参数会将传入的参数挂在到jq的全局对象
    
    	b. $.extend({
		        hello:{}
		    })//在全局新建了一个叫做hello的命名空间

		c. $.extend($.hello,{xxx:xxx})//挂载到hello的命名空间中
	       $.fn.extend({})挂载到实例对象上，既每个不同的$(".cls")对象
	```

3. jq插件写法，挂载到$上和$.fn上，第一种是全局，第二种是jq对象，并且能够使用jq选择器 https://www.cnblogs.com/ajianbeyourself/p/5815689.html 需要参数接收，链式调用 
```
	$.fn.my=function(opt){
	        var default={

	        };
	        var _opt=$.extend({},default,opt);

	        // do sth

	        return this
	    }
     
    	//一种oop并且不污染全局变量的方式:--注意分号的使用技巧
	    ;(function(){
	        var My=function(elm,opt){};
	        My.prototype={
	            // do sth
	        }

	        $.fn.my=function(opt){
	            var _my=new My(sth);

	            return sth;
	        }
	    })()

	    //为了缩短定义域查找链:

	    ;(function($,window,document,undefined){

	    })(jQuery,window,document)
	```