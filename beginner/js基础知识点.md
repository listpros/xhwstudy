# js基础知识点 By [nickzone](https://github.com/nickzone/)

> **初级难度** 

## 学习资源相关链接
1. 文档型：

  * [MDN](https://developer.mozilla.org/zh-CN/docs/Web)
  * [MDN/js](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide)
  * [es6@阮一峰](http://es6.ruanyifeng.com/)
  * [ecscript-org](http://www.ecma-international.org/ecma-262/6.0/)
  
2. 问答型：
  * https://segmentfault.com
  * Stack Overflow
  * zhihu.com

3. 书籍型：

  * CSS权威指南
  * JavaScript高级程序设计
  * javascript语言精粹
  * 编写可维护的javascript
  * js面向对象精要
  
4. 百度型：（不推荐）
  * 各类博客、技术文章（质量参差不齐，要有很强的鉴别能力；碎片化，片面化导致的似懂非懂）
  
## 变量声明提升与作用域

1. [变量声明提升](https://developer.mozilla.org/zh-CN/docs/Glossary/Hoisting) 
2. [作用域](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Grammar_and_types#%E5%8F%98%E9%87%8F%E7%9A%84%E4%BD%9C%E7%94%A8%E5%9F%9F)

## 运算符及其优先级 

1. [运算符类型](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators)
2. [运算符优先级](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Operator_Precedence)

## 数据类型和隐式转换

1. [数据类型](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Grammar_and_types#%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E5%92%8C%E7%B1%BB%E5%9E%8B)
  
2. 数据类型判断 [typeof](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/typeof) 
  
3. 隐式类型转换 [相等判断](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Equality_comparisons_and_sameness)

## 函数

1. 变量作用域链
2. [this](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/this)
3. [闭包](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Closures)

## 异步与回调

1. [事件队列](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/EventLoop)
2. [异步编程](http://www.ruanyifeng.com/blog/2012/12/asynchronous%EF%BC%BFjavascript)

## 面向对象

1. [继承与原型链](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Inheritance_and_the_prototype_chain)
2. 模块化： [基本](https://developer.mozilla.org/zh-CN/docs/Glossary/%E7%AB%8B%E5%8D%B3%E6%89%A7%E8%A1%8C%E5%87%BD%E6%95%B0%E8%A1%A8%E8%BE%BE%E5%BC%8F),[高级](https://segmentfault.com/a/1190000000733959)

## 设计模式

1. [mvc](https://developer.mozilla.org/zh-TW/Apps/Fundamentals/Modern_web_app_architecture/MVC_architecture)
