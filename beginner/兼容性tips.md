# 兼容性tips By [hover-hou](https://github.com/nickzone/)

> **初级难度** 

本课件涉及一些css兼容性为
#### h5
- 我理解的h5：h5是h4的拓展和增强,不局限于标签，包含各类功能接口等全方位增强，交互性更强，移动端友好
- h5专题/h5页面：什么是h5专题
- h5 与移动端联系：h5增强了适配，能在不同尺寸的小屏幕上适当调整缩放以展示页面

#### css2.* css3
- 动画(含transition)，http://www.w3school.com.cn/css3/css3_animation.asp
- 选择器，http://www.w3school.com.cn/cssref/css_selectors.asp
- 圆角，阴影，多背景，渐变 w3school

#### 兼容特性
- 如何判断兼容特性 
    - mdn: https://developer.mozilla.org/zh-CN/docs/Web/Guide
    - caniuse: https://www.caniuse.com

- ie8和 模拟器
    - ie8 版本(win7原版和升级版)
    - ie 自带的模拟器
    - ietester
- 兼容性处理
	- autoprefixer插件
    - 优雅降级: 尽量保留内容的情况下 削减不能兼容的部分
    - 有好的用户提示：如果不能保留内容或者兼容成本过大时
#### chrome与360浏览器
- 360兼容模式和极速模式
- 始360以ie最高版本渲染页面：``` <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE11"> ```
- 始360优先以blink渲染页面：``` <meta name="renderer" content="webkit"> ```

#### 移动端以及响应式
- 媒体查询
- 高清屏--物理分辨率和显示分辨率
- rem,vw,vh
- bootstrap 3/4 http://www.bootcss.com/
