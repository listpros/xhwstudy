# 代码习惯小tip By [hover-hou](https://github.com/nickzone/)

> **初级难度** 

本课件提示一些代码小习惯
#### 样式 结构 行为 分离
- 不要将js，css写在dom里，应尽量外链
- 库文件的位置
   
#### 写注释
- 易于维护的注释，
- 区分结构的注释，
- 说明性的注释

#### 注意命名空间
- 什么是命名空间
- js的命名空间
- css的'命名空间'
- css'模块化'-- BEM https://www.w3cplus.com/blog/tags/325.html https://www.w3cplus.com/css/bem-definitions.html

#### js的书写习惯
- 模块化
- 功能拆分，减少耦合，提高复用
- 代码顺序，自顶向下
- var 尽量集中放置
